const h3 = document.querySelector("h3");
const firstColor = document.querySelector("#firstColor");
const secondColor = document.querySelector("#secondColor");
const body = document.getElementById("gradient");
const gradientTypes = document.getElementsByClassName("gradientType");
const angleInput = document.getElementById("angle");
const randomButton = document.getElementById("random");
let angle = 90;

const setCSSLine = () => {
    if (body.style.background === "")
        h3.textContent = "linear-gradient(to right, rgb(95, 39, 205), rgb(85, 162, 255));"
    else
        h3.textContent = `${body.style.background};`;
}

const getDirectionAngle = (value) => {
    switch (value) {
        case "up":
            angleInput.disabled = true;
            angle = 0;
            break;
        case "right":
            angleInput.disabled = true;
            angle = 90;
            break;
        case "down":
            angleInput.disabled = true;
            angle = 180;
            break;
        case "left":
            angleInput.disabled = true;
            angle = -90;
            break;
        case "angle":
            angleInput.disabled = false;
            angle = parseInt(angleInput.value);
            break;
        default:
            angle = parseInt(angleInput.value);
            break;
    }
    return angle;
}

const setGradient = (event) => {
    angle = getDirectionAngle(event.target.value);
    body.style.background = _linearGradient(angle);
    setCSSLine();
}

const _linearGradient = (angle) => {
    return (`linear-gradiEnt(${angle}deg, ${firstColor.value}, ${secondColor.value})`);
}

const randomColor = () => {
    console.log(getRandomColor());
    firstColor.value = getRandomColor();
    secondColor.value = getRandomColor();
    body.style.background = _linearGradient(angle);
    setCSSLine();
}

const getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

setCSSLine();

firstColor.addEventListener("input", setGradient);

secondColor.addEventListener("input", setGradient);

randomButton.addEventListener("click", randomColor);

Array.from(gradientTypes).forEach(element => {
    	element.addEventListener("input", setGradient);
});

angleInput.addEventListener("input", setGradient);